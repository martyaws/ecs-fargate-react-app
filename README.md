# JavaScript Docker React
Simple project that runs a React app in a Docker container.  [Docker Desktop](https://www.docker.com/products/docker-desktop/) is your friend!

# YouTube
Watch [this](https://youtu.be/vJpeP429UIc) YouTube video from yours truly.

# Getting Started
- Clone this rep
- Install dependencies: `npm install`
- Run the app w/o a container: `npm start`

# Docker Notes
- Containers are running instances of docker images
- The `Dockerfile` is used to create the docker image
- View list of local Docker images: `docker images`
- Run the Docker image as a container: `docker run -p 80:3000 react-app-image`
- List the running Docker containers: `docker ps`
- Kill container: `docker kill {goofy name} | {container id}`

# Docker Workflow
- Commit your code
- Use semantic versioning or simply 'latest'
- `docker build -t react-app-image:{version number} .`
- `docker tag react-app-image:{version number} mburolla/react-app:{version number}`
- `docker login`
- `docker push mburolla/react-app:{version number}`
- Pull and test using Docker Desktop

# AWS EC2 Notes
### Installation
- [Run docker on EC2 Linux](https://geeksterminal.com/install-docker-on-ec2-linux-ami-centos-rhel-fedora/2612/)
- [Docker Docs](https://docs.docker.com/engine/install/centos/)
- `sudo yum update -y`
- `sudo yum install docker -y`
- `docker --version`
- `docker info`
- `sudo su`
- Run docker: `sudo service docker start`
- Background: `sudo service docker start >/dev/null 2>&1 &`

### Updating EC2
- `docker stop <goofy name>`
- `docker rm <goofy name>`
- `docker pull mburolla/react-app:latest`
- `docker run -d -p 80:3000 mburolla/react-app:latest`
