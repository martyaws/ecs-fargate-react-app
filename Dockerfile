# Use an official Node.js runtime as the base image
FROM node:14-alpine

# Set the working directory inside the container
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install app dependencies
RUN npm install

# Copy the entire app to the container
COPY . .

# Build the React app
RUN npm run build

# Expose the container port
EXPOSE 3000

# Start the React app when the container launches
CMD ["npm", "start"]
